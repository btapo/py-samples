import openpyxl

from py36.boot import AppConfig

if __name__ == '__main__':
    workbook = openpyxl.load_workbook(AppConfig.get_data_in_file("sample_excel.xlsx"))
    sheet = workbook.get_sheet_by_name('Sheet1')
    for rowOfCellObjects in sheet['A1':'C3']:
        for cellObj in rowOfCellObjects:
            print(cellObj.coordinate, cellObj.value)
    print('--- END OF ROW ---')
