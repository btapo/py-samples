import logging
import os

logging.basicConfig(filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO)


class AppConfig(object):
    @staticmethod
    def get_home():
        return "/home/tapo/PycharmProjects/py-sample"

    @staticmethod
    def get_data_in_file(file):
        return AppConfig.get_home() + "/data/in/" + file
